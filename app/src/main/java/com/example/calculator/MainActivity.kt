package com.example.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private var firstvariable = 0.0
    private var secondvariable = 0.0
    private var operation = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init(){
        buttonN0.setOnClickListener(this)
        buttonN1.setOnClickListener(this)
        buttonN2.setOnClickListener(this)
        buttonN3.setOnClickListener(this)
        buttonN4.setOnClickListener(this)
        buttonN5.setOnClickListener(this)
        buttonN6.setOnClickListener(this)
        buttonN7.setOnClickListener(this)
        buttonN8.setOnClickListener(this)
        buttonN9.setOnClickListener(this)

        buttonNdot.setOnClickListener {
            resultTextView.text = resultTextView.text.toString() + "."
            if (resultTextView.text.contains(".")){
                buttonNdot.setOnClickListener { false }
            }
        }

    }

    fun equal(view: View){
        val value = resultTextView.text.toString()
        if (value.isNotEmpty()) {
            secondvariable = value.toDouble()

            var result: Double = 0.0
            if (operation == ":") {
                result = firstvariable / secondvariable
            }
            else if (operation == ":" && secondvariable == 0.0){
                Toast.makeText(this, "Can't divide by zero", Toast.LENGTH_SHORT).show()
            }
            else if (operation == "+") {
                result = firstvariable + secondvariable
            }
            else if (operation == "-") {
                result = firstvariable - secondvariable
            }
            else if (operation == "x") {
                result = firstvariable * secondvariable
            }
            resultTextView.text = result.toString()
        }
    }

    fun divide(view: View){
        val value = resultTextView.text.toString()
        if (value.isNotEmpty()) {
            operation = ":"
            firstvariable = value.toDouble()
            resultTextView.text = ""
        }
    }

    fun minus(view: View){
        val value = resultTextView.text.toString()
        if (value.isNotEmpty()){
            operation = "-"
            firstvariable = value.toDouble()
            resultTextView.text = ""
        }
    }

    fun plus(view: View){
        val value = resultTextView.text.toString()
        if (value.isNotEmpty()){
            operation = "+"
            firstvariable = value.toDouble()
            resultTextView.text = ""
        }
    }

    fun times(view: View){
        val value = resultTextView.text.toString()
        if (value.isNotEmpty()){
            operation = "x"
            firstvariable = value.toDouble()
            resultTextView.text = ""
        }
    }

    fun delete(view: View){
        val value = resultTextView.text.toString()
        if (value.isNotEmpty())
            resultTextView.text = value.substring(0, value.length - 1)
        buttonDelete.setOnLongClickListener {
            resultTextView.text = value.substring(0, 0)
            true
        }
    }

    override fun onClick(v: View?) {
        val button = v as Button
            resultTextView.text = resultTextView.text.toString() + button.text.toString()


    }

}